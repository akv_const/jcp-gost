package ru.taxcom.asn1.cms;

import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;

/**
 * <a href="https://tc26.ru/upload/iblock/2c0/CMS%20(%D0%BF%D0%B5%D1%80%D0%B2%D0%B0%D1%8F%20%D1%80%D0%B5%D0%B4%D0%B0%D0%BA%D1%86%D0%B8%D1%8F)_v1.pdf">
 *     Using algorithms GOST R 34.12-2015, GOST R 34.13-2015, GOST R 34.10-2012 and GOST R 34.11-2012 in CMS
 * </a>:
 * Transport key format.
 * <pre>
 * GostR3410-KeyTransport ::= SEQUENCE {
 *     encryptedKey OCTET STRING,
 *     ephemeralPublicKey SubjectPublicKeyInfo,
 *     ukm OCTET STRING
 * }
 * </pre>
 */

public class Gost3410_2015_KeyTransport extends ASN1Object {
    private final byte[] encryptedKey;
    private final SubjectPublicKeyInfo ephemeralPublicKey;
    private final byte[] ukm;

    public Gost3410_2015_KeyTransport(byte[] encryptedKey, SubjectPublicKeyInfo ephemeralPublicKey, byte[] ukm) {
        this.encryptedKey = encryptedKey;
        this.ephemeralPublicKey = ephemeralPublicKey;
        this.ukm = ukm;
    }

    public Gost3410_2015_KeyTransport(ASN1OctetString encoded) {
        ASN1Sequence sequence = ASN1Sequence.getInstance(encoded.getOctets());
        ASN1Encodable encodedField = sequence.getObjectAt(0);
        if (!(encodedField instanceof ASN1OctetString))
            throw new IllegalArgumentException("Encrypted key must be an ASN1OctetString type");
        encryptedKey = ((ASN1OctetString) encodedField).getOctets();
        ephemeralPublicKey = SubjectPublicKeyInfo.getInstance(sequence.getObjectAt(1));
        encodedField = sequence.getObjectAt(2);
        if (!(encodedField instanceof ASN1OctetString))
            throw new IllegalArgumentException("ukm must be an ASN1OctetString type");
        ukm = ((ASN1OctetString) encodedField).getOctets();
    }

    public static Gost3410_2015_KeyTransport getInstance(Object obj) {
        if (obj == null)
            return null;
        else if (obj instanceof Gost3410_2015_KeyTransport)
            return (Gost3410_2015_KeyTransport)obj;
        else if (obj instanceof ASN1OctetString) {
            return new Gost3410_2015_KeyTransport((ASN1OctetString)obj);
        }
        throw new IllegalArgumentException("unknown object in getInstance: " + obj.getClass().getName());
    }

    public byte[] getEncryptedKey() {
        return encryptedKey;
    }

    public SubjectPublicKeyInfo getEphemeralPublicKey() {
        return ephemeralPublicKey;
    }

    public byte[] getUkm() {
        return ukm;
    }

    @Override
    public ASN1Primitive toASN1Primitive() {
        ASN1EncodableVector vector = new ASN1EncodableVector();
        vector.add(new DEROctetString(encryptedKey));
        vector.add(ephemeralPublicKey);
        vector.add(new DEROctetString(ukm));
        return new DERSequence(vector);
    }
}
