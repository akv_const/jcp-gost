package ru.taxcom.jcsptest;

import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.EnvelopedData;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;
import org.bouncycastle.asn1.cms.RecipientInfo;
import ru.CryptoPro.Crypto.CryptoProvider;
import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCP.params.G3412ParamsSpec;
import ru.CryptoPro.JCP.params.Kexp15ParamsSpec;
import ru.taxcom.asn1.cms.Gost3410_2015_KeyTransport;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.spec.IvParameterSpec;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.spec.X509EncodedKeySpec;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.ServiceLoader;

public class Application {
    public static void Decrypt(String containerName, String encFileName, String decFileName) {
        try {
            // Загружаем закрытый ключ
            KeyStore keyStore = KeyStore.getInstance("HDImageStore", JCP.PROVIDER_NAME);
            keyStore.load(null, null);
            var privateKey = (PrivateKey)keyStore.getKey(containerName, "".toCharArray());
            if (privateKey == null)
                throw new RuntimeException("Cannot find private key.");
            // Загружаем CMS
            var contentInfo = ContentInfo.getInstance(ASN1Primitive.fromByteArray(Files.readAllBytes(Path.of(encFileName))));
            var envelopedData = EnvelopedData.getInstance(contentInfo.getContent());
            var start = Instant.now();
            // Расшифровываем сессионный ключ
            var recipientInfo = (KeyTransRecipientInfo) RecipientInfo.getInstance(envelopedData.getRecipientInfos().getObjectAt(0)).getInfo();
            var keyTransport = Gost3410_2015_KeyTransport.getInstance(recipientInfo.getEncryptedKey());
            // Получаем эфемерный ключ
            var keyFactory = KeyFactory.getInstance(JCP.GOST_DH_2012_256_NAME);
            var publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(keyTransport.getEphemeralPublicKey().getEncoded()));
            // Ключ согласования
            byte[] iv = new byte[JCP.CMS_GR3412_KEG_UKM_LEN];
            for (int i = 0; i < JCP.CMS_GR3412_KEG_UKM_LEN; ++i) {
                iv[i] = keyTransport.getUkm()[JCP.CMS_GR3412_KEG_UKM_LEN - i - 1 ];
            }
            var keyAgreement = KeyAgreement.getInstance(JCP.GOST_DH_2012_256_NAME);
            keyAgreement.init(privateKey, new IvParameterSpec(iv));
            keyAgreement.doPhase(publicKey, true);
            var secretKey = keyAgreement.generateSecret(CryptoProvider.GOST_M_CIPHER_NAME);
            // Параметры шифрования ключа
            var baseUkm = Arrays.copyOfRange(keyTransport.getUkm(), JCP.CMS_GR3412_KEXP15_IV_OFFSET,
                    JCP.CMS_GR3412_KEXP15_IV_OFFSET + JCP.G28147_BLOCKLEN / 2);
            var extendedUkm = Arrays.copyOfRange(keyTransport.getUkm(), JCP.CMS_GR3412_KEG_UKM_LEN,
                    JCP.CMS_GR3412_KEG_UKM_LEN + JCP.CMS_GR3412_KEG_SEED_LEN);
            // Расшифровываем
            var cipher = Cipher.getInstance(CryptoProvider.GOST_M_CIPHER_NAME + "/KEXP_2015_M_EXPORT/NoPadding");
            cipher.init(Cipher.UNWRAP_MODE, secretKey, new Kexp15ParamsSpec(baseUkm, extendedUkm));
            var sessionKey = cipher.unwrap(keyTransport.getEncryptedKey(), null, Cipher.SECRET_KEY);
            // Расшифровываем данные
            // Параметры
            var encryptionUkm = ((ASN1OctetString)((ASN1Sequence)envelopedData.getEncryptedContentInfo().getContentEncryptionAlgorithm().getParameters()).getObjectAt(0)).getOctets();
            // Расшифровываем
            cipher = Cipher.getInstance(CryptoProvider.GOST_M_CIPHER_NAME + "/OMAC_CTR/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, sessionKey, new G3412ParamsSpec(encryptionUkm, true));
            var decrypted = cipher.doFinal(envelopedData.getEncryptedContentInfo().getEncryptedContent().getOctets());
            var finish = Instant.now();
            System.out.println("Decryption time: " + Duration.between(start, finish).toMillis() + " ms");
            // Сохраняем результат
            Files.write(Paths.get(decFileName), decrypted, StandardOpenOption.CREATE);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String inFileName = null;
        String outFileName = null;
        String containerName = null;
        for (int i = 0; i < args.length; ++i) {
            switch (args[i]) {
                case "-in":
                    inFileName = args[++i];
                    break;
                case "-out":
                    outFileName = args[++i];
                    break;
                case "-container":
                    containerName = args[++i];
                    break;
            }
        }
        if (containerName == null) {
            System.out.println("Set key container name.");
            return;
        }
        if (inFileName == null) {
            System.out.println("Set input file name.");
            return;
        }
        if (outFileName == null) {
            System.out.println("Set output file name.");
            return;
        }
        var sl = ServiceLoader.load(java.security.Provider.class);
        for (var p : sl) {
            if (p.getName().equals("JCP") || (p.getName().equals("Crypto"))) {
                Security.addProvider(p);
            }
        }
        Decrypt(containerName, inFileName, outFileName);
    }
}
